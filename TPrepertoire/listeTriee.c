#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <string.h>

int compare(const void * a, const void * b);

int main()
{
    DIR *Directory = opendir(".");

    int nbSon=0;

    if(Directory != NULL)
    {
        struct dirent *current =NULL;

        current = readdir(Directory);

        while(current != NULL)
        {
            nbSon++;

            current = readdir(Directory);
        }

        char** TE = malloc(sizeof(char*) * nbSon);

        rewinddir(Directory);


        current = readdir(Directory);

        int i = 0;
        
        while(current != NULL)
        {
             char *sonName = malloc(sizeof(char) * strlen(current->d_name)+1);
             strcpy(sonName,current->d_name);
             TE[i] = sonName;
             i++;
             current = readdir(Directory);
        }
        qsort(TE,nbSon,sizeof(char*),compare);
        
        int j;
        for(j=0;j<nbSon;j++)
        {
             printf("%s\n",TE[j]);
             
             free (TE[j]);
        }
        
        free (TE);
    }
    
    closedir(Directory);
    return 0;
}

int compare(const void * a, const void * b)
{
    //compare names

    char* c = *(char**)a;
    char* d = *(char**)b;

    return strcmp(c,d);
}

