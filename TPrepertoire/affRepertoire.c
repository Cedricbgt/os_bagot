#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>

//Function prototypes:
void affDirectory(char *DirectoryName);

int main(int nbArgs, char * args[])
{
    int i;
    for(i =1;i<nbArgs;i++)
    {
        affDirectory(args[i]);
    }
    return 0;
}

void affDirectory(char *DirectoryName)
{
    
    DIR *Directory = opendir(DirectoryName);

    //If opening succeeded
    if(Directory != NULL)
    {
        
        printf("%s : \n",DirectoryName);
        struct dirent *current =NULL;
        
        current = readdir(Directory);
        
        while(current != NULL)
        {
            
            printf("%s %ld \n",current->d_name,current->d_ino);

            
            current = readdir(Directory);
        }

    }
    printf("\n");

    
    closedir(Directory);
}