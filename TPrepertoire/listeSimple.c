#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>




int main()
{
        DIR *rep;
        rep = opendir(".");
        if (rep != NULL)
        {
            struct dirent *courant;
            courant = readdir(rep);
            while ((courant = readdir(rep)) != NULL)
            {
                printf("%s, %ld\n", courant->d_name, courant->d_ino);
            }
            closedir(rep);
        }
        return 0;
}


