#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

#define PATHDIR "."

typedef struct File File;
struct File {
    // Element name
    char *fileName;
    // Element size
    struct stat fstat;
};

//Compare function for qsort 
int compare(const void *a, const void *b);

int main()
{
    // Open DIRECTORY_RELATIVE_PATH directory
    DIR *Directory = opendir(PATHDIR);

    // Counter of sub elements
    int nbSon = 0;

    if (Directory != NULL) {
        struct dirent *current = NULL;

        // Get directory first element
        current = readdir(Directory);

        while (current != NULL) {
            nbSon++;
            current = readdir(Directory);
        }

        // Allocate an array of file information
        File *TE = malloc(sizeof(File) * nbSon);

        // Get back to the first element
        rewinddir(Directory);

        // Get directory first element
        current = readdir(Directory);
        int i = 0;

        while (current != NULL) {

            TE[i].fileName = malloc(sizeof(char) * (strlen(current->d_name) + 1));

            strcpy(TE[i].fileName, current->d_name);

            char buff[100];

            sprintf(buff, "%s\\%s", PATHDIR, current->d_name);

            struct stat fstat;

            stat(buff, &fstat);
            TE[i].fstat = fstat;
            i++;

            current = readdir(Directory);

        }

        qsort(TE, nbSon, sizeof(File), compare);
        int j;

        for (j = 0; j < nbSon; j++) {
            printf("%s %d\n", TE[j].fileName, TE[j].fstat.st_size);

            // free the element name allocated memory
            free(TE[j].fileName);
        }

        // free the allocated file information array
        free(TE);
    }
    closedir(Directory);

    return 0;
}

int compare(const void *a, const void *b)
{

    File c = *(File *) a;
    File d = *(File *) b;

    return strcmp(c.fileName, d.fileName);
}
