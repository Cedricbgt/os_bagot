/* --------------------------------------------------------------------------- 
 * Schéma de création de processus : création d'un fils, les deux processus
 * affichent simplement leur PID et PPID.
 * Auteur(s)    :
 * Groupe de TP :
 * --------------------------------------------------------------------------*/

#include <stdio.h>    /* Pour utiliser printf, perror, NULL... */
#include <stdlib.h>    /* Pour exit */
#include <unistd.h>    /* Pour fork, getpid, getppid */
#include <errno.h>
#include <sys/types.h>    /* Pour pid_t */
#include <sys/wait.h>    /* Pour wait */

extern int errno;    /* Modifiée en cas d'erreur */

/* Fonctions exécutant le code propre au père ou au fils */
void pere(void);
void fils(void);

/* ---------------------------------------------------------------------------
 * Création d'un processus fils et exécution d'une fonction particulière
 * par chaque processus (père et fils).
 */
int main(void)
{
  pid_t ident;

  ident = fork();

  /* A partir de cette ligne, deux processus exécutent le code */
  printf("Cette ligne va être affichée deux fois\n");

  switch (ident) {
     case -1:
        perror("fork");
        return errno;
     case 0:
        /* Code exécuté uniquement par le fils */
        fils();
        return EXIT_SUCCESS;  /* Garantit que le fils ne fait que la *
                               * fonction fils() (on aurait pu aussi *
                               * mettre un else au if) */
        break;
     default:
        /* Code exécuté uniquement par le père, ici ident == identification du 
         * fils crée */
                 ident = fork();
        switch(ident) {
            case 0:
            fils();
              return EXIT_SUCCESS;
        break;
     default:
            

        pere();
}
        
}
  return EXIT_SUCCESS;
}


/* Actions du processus père, regroupées dans une procédure. */
void pere(void)
{
wait(NULL);
printf("Pere --> PID = %d - PPID = %d\n", getpid(), getppid());
}

/* Actions du processus fils */
void fils(void)
{ sleep(5);
  printf("Fils --> PID = %d - PPID = %d\n", getpid(), getppid());
}

