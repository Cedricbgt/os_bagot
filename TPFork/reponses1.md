1.1 Le PPID du fils est égal au PID du père même si on relance plusieurs fois 

1.2 On a lancé fork plusieurs fois après avoir effectuer le sleep et on 
a remarqué que le PPID du fils reste inchangé lorsque son père "naturel" 
se termine avant lui

1.3 Le processus père du père correspond au bash (PID = 1574) 
et le processus père du fils correspond au système (/lib/systemd/systemd/ --user) (PID = 1257)

