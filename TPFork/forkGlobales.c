/* --------------------------------------------------------------------------- 
 * Schéma de création de processus : création d'un fils, les deux processus
 * affichent simplement leur PID et PPID.
 * Auteur(s)    :
 * Groupe de TP :
 * --------------------------------------------------------------------------*/

#include <stdio.h>              /* Pour utiliser printf, perror, NULL... */
#include <stdlib.h>             /* Pour exit */
#include <unistd.h>             /* Pour fork, getpid, getppid */
#include <errno.h>
#include <sys/types.h>          /* Pour pid_t */
#include <sys/wait.h>           /* Pour wait */

int Countglo = 0;
void fils();
int main(void)
{
    pid_t ident;

    ident = fork();


    switch (ident) {
    case -1:
        perror("forkerror");
    break;
    case 0:
        fils();
    break;
    default:
        while(Countglo<10000)
        {
            printf("pere =>%d\n",Countglo++);
        }
    break;    
    }
    return 0;
}



/* Actions du processus fils */
void fils(void)
{
    while(Countglo<10000){
        printf("Fils =>%d\n",Countglo++);
    }
}
