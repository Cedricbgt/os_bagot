/* ---------------------------------------------------------------------------
 * Schéma de programme pour le TP sur les tubes (redirection des E/S standard)
 *     On créé un tube puis un fils, puis on chaîne la sortie standard du
 *     fils sur l'entrée standard du père via le tube créé.
 * Auteur(s) :
 * Groupe TP : 
 * Entrées   : aucune
 * Sorties   : traces à l'écran
 * --------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>          /* Pour pid_t (fork, getpid, getppid) */

extern int errno;               /* Modifiée en cas d'erreur */

int main(int argc, char** argv)
{
   pid_t ident;
   int tube[2];                 /* les deux descripteurs de fichier associés au tube */
   pid_t ident2;
   int tube2[2];               


   /* Création du tube */
   if (pipe(tube) != 0 || pipe(tube2) != 0) {
      perror("pipe");
      return EXIT_FAILURE;
   }

   /* Création du fils */
   ident = fork();
   switch (ident) {
      case -1:
         perror("fork");
         return EXIT_FAILURE;

      case 0: /* Fils */
         /* Redirige la sortie standard sur le tube */
         dup2(tube[1], 1);        /* 1 <- tube[1] */
         close(tube[0]);          /* le fils ne lit pas dans le tube */
         close(tube2[1]);
         close(tube2[0]);

         /* Ici on exécute un programme dont la sortie sera redirigée sur le tube, exemple : */
         execlp("who", "who", "/bin", NULL);
         break;

      default: /* Père */
        ident2 = fork();
        switch(ident2){
        
            case-1:
                perror("fork");
                return EXIT_FAILURE;
                
            case 0:
                dup2(tube[0],0);          /* 0 <- tube[0] */
                dup2(tube2[1],1);
                close(tube[1]);           /* le père n'écrit pas dans le tube */
                close(tube2[0]);

                /* Ici on exécute un programme dont l'entrée sera redirigée sur le tube, exemple : */
                 execlp("cut","cut","-c","1-8",NULL);
                 break;
            
            default :
                dup2(tube2[0],0);
                close(tube[1]);           /* le père n'écrit pas dans le tube */
                close(tube[0]);
                close(tube2[1]);
                /* Ici on exécute un programme dont l'entrée sera redirigée sur le tube, exemple : */
                 execlp("sort","sort","-u",NULL);
                 break;}
     
   }

   return EXIT_SUCCESS;
}

