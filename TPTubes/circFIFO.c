/* ---------------------------------------------------------------------------
 * Question 2 du TP sur les tubes Unix.
 * 1 : on crée un tube puis un processus fils. Le fils envoie un message
 *     au père dans le tube.
 * Auteur(s) : 
 * Groupe TP :
 * Entrées   : aucune
 * Sorties   : traces à l'écran
 * --------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>          /* Pour pid_t (fork, getpid, getppid) */

#define MAX 64   /* Taille des tampons de communication */

int main(void)
{
   pid_t ident;
   pid_t ident2;
   int tubePereFils1[2];                 /* les deux descripteurs associés au tube */
   int tubeFils1Fils2[2];                 /* les deux descripteurs associés au tube */
   int tubeFils2Pere[2];                 /* les deux descripteurs associés au tube */
   char message[MAX];           /* Une chaîne pour stocker les messages lus dans le tube */

   /* Création du tube */
   if ((pipe(tubePereFils1) || pipe(tubeFils1Fils2) || pipe(tubeFils2Pere))  != 0) {
      perror("Pipe creation failed.");
      return EXIT_FAILURE;
   }

   /* Création du fils */
   ident = fork();
   switch (ident) {
   case -1:
      perror("fork");
      return EXIT_FAILURE;

   case 0:                     /* Fils */
      close(tubePereFils1[1]);
      close(tubeFils2Pere[0]);
      close(tubeFils1Fils2[0]);
      close(tubeFils2Pere[1]);
      printf("Fils1 : J'écris 'Bonjour mon frere...' dans le tube\n");
      write(tubeFils1Fils2[1], "Bonjour mon frere...", 11); /* On écrit le '\0' de fin */
      
      read(tubePereFils1[0], message, MAX);
      printf("Fils1 : Je lis dans le tubePereFils1\n");
      printf("Fils1 : J'ai lu %s\n", message);
      break;

   default:                    /* Père */
        switch(ident2) {
        case -1:
            perror("fork");
            return EXIT_FAILURE;
        
        case 0:
            close(tubePereFils1[0]);
            close(tubePereFils1[1]);
            close(tubeFils2Pere[0]);
            close(tubeFils1Fils2[1]);
            sleep(2);
            printf("Fils2: Je lis le tube\n");
            read(tubeFils1Fils2[0], message, MAX);
            printf("%s \n",message);
            printf("Fils2 : j'écris 'ca va ?' dans le tube\n");
            write(tubeFils2Pere[1], "Ca va?", 11); /* On écrit le '\0' de fin */
            break;
            
        default:
            close(tubePereFils1[0]);
            close(tubeFils2Pere[0]);
            close(tubeFils1Fils2[0]);
            close(tubeFils1Fils2[1]);
            printf("Père : je lis un message dans le tube\n");
            read(tubeFils2Pere[0], message, MAX);
            printf("Père : j'ai lu %s\n", message);
            printf("Père : J'écris dans le tube\n");
            write(tubePereFils1[1], "Audi RS5", 11);
        }
   }

   return EXIT_SUCCESS;
}