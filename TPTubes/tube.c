/* ---------------------------------------------------------------------------
 * Question 1 du TP sur les tubes Unix.
 * 1 : on crée un tube puis un processus fils. Le fils envoie un message
 *     au père dans le tube.
 * Auteur(s) : 
 * Groupe TP : 
 * Entrées   : aucune
 * Sorties   : traces à l'écran
 * --------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>          /* Pour pid_t (fork, getpid, getppid) */

#define MAX 64   /* Taille des tampons de communication */

int main(void)
{
   pid_t ident;
   int tube[2];                 /* les deux descripteurs associés au tube */
   char message[MAX];           /* Une chaîne pour stocker les messages lus dans le tube */

   /* Création du tube */
   if (pipe(tube) != 0) {
      perror("Pipe creation failed.");
      return EXIT_FAILURE;
   }

   /* Création du fils */
   ident = fork();
   switch (ident) {
   case -1:
      perror("fork");
      return EXIT_FAILURE;

   case 0:                     /* Fils */
      printf("Fils : j'écris 'Bonjour...' dans le tube\n");
      sleep(2);
      write(tube[1], "Bonjour...", 11); /* On écrit le '\0' de fin */
      break;

   default:                    /* Père */
      printf("Père : je lis un message dans le tube\n");
      printf("Père : j'attends ...\n");
      read(tube[0], message, MAX);
      printf("Père : j'ai lu '%s'\n", message);
   }

   return EXIT_SUCCESS;
}

