/* ---------------------------------------------------------------------------
 * Programme affichant une voiture qui parcours une route
 * Auteur(s)  :
 * Groupe TP  :
 * Entrées    : Aucune
 * Sorties    : Affichage d'une fenêtre graphique
 * Avancement : <Où en êtes vous ? Qu'est-ce qui fonctionne ?>
 */

#include "road.h"

#define _GNU_SOURCE             // For pthread_tryjoin_np
#include <unistd.h>             // For usleep
#include <pthread.h>            // For threading in general
#include <errno.h>              // For EBUSY
#include <stdlib.h>             // For EXIT_FAILURE
#include <stdio.h>              // Just in case

static void *avancer(void *id)
{
    int carId = *(int *) id;

    if (carId == -1) {
        fprintf(stderr, "Something went wrong when trying to add a car on the road. Exiting.\n");
        return (void *) -1;
    }
    int ret = 1;

    while (!road_isEscPressed() && ret) {
        usleep(10000);
        ret = road_stepCar(carId);
    }
    road_removeCar(carId);
    return (void *) 0;
}

int main(int argc, const char *argv[])
{
    pthread_t t;

    road_init();
    int carId = road_addCar();

    pthread_create(&t, NULL, avancer, (void *) &carId);

    while (!road_isEscPressed() && pthread_tryjoin_np(t, NULL)) {
        road_refresh();
    }

    road_shutdown();

    return 0;
}
