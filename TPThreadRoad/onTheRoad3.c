/* ---------------------------------------------------------------------------
 * Programme affichant une voiture qui parcours une route
 * Auteur(s)  :
 * Groupe TP  :
 * Entrées    : Aucune
 * Sorties    : Affichage d'une fenêtre graphique
 * Avancement : <Où en êtes vous ? Qu'est-ce qui fonctionne ?>
 */

#include "road.h"

#define _GNU_SOURCE  // For pthread_tryjoin_np
#include <unistd.h>  // For usleep
#include <pthread.h> // For threading in general
#include <errno.h>   // For EBUSY
#include <stdlib.h>  // For EXIT_FAILURE
#include <stdio.h>   // Just in case



static void * avancer(void *id)
{
    int carId = *(int *)id;

   int ret = 1;

    while (!road_isEscPressed() && ret)
   {
      usleep(100);
      ret = road_stepCar(carId);
   }
    road_removeCar(carId);
   

   return (void *)0;
}

static void* creerVoiture()
{
    while(!road_isEscPressed())
    {
        int carId = road_addCar();
        if (carId != -1)
       {
            pthread_t vehicul;
             pthread_create(&vehicul,NULL,avancer,(void *)&carId);
             sleep(rand()%1);
       }
    }
    return (void *)0;
}

int main(int argc, const char *argv[])
{
    pthread_t t;


   road_init();
    pthread_create(&t,NULL,creerVoiture,NULL);


   while (!road_isEscPressed())
   {
      road_refresh();
   }

    pthread_join(t,NULL);

   road_shutdown();

   return 0;
}
