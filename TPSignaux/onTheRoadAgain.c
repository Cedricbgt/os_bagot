
#define _GNU_SOURCE
#include <unistd.h> // For usleep
#include <pthread.h>
#include <stdlib.h> // For rand
#include <signal.h> // For signals
#include <sys/time.h> // For setitimer
#include <stdio.h>

#include <road.h>

static void * avancer(void *id)
{
    int carId = *(int *)id;

   int ret = 1;

    while (!road_isEscPressed() && ret)
   {
      usleep(1000);
      ret = road_stepCar(carId);
   }
    road_removeCar(carId);
   

   return (void *)0;
}

static void creerVoiture()
{
    int carId = road_addCar(0);
    pthread_t vehicul;
    pthread_create(&vehicul,NULL,avancer,&carId);
}

int main(int argc, const char *argv[])
{
   road_init(0);
   struct sigaction sa;
   sa.sa_handler = creerVoiture;
   sigaction(SIGTSTP, &sa, NULL);
   
   int carId1 = road_addCar(0);
   if (carId1 == -1) {
      fprintf(stderr, "Something went wrong when trying to add a car on the road. Exiting.\n");
      return EXIT_FAILURE;
   }

   int carId2 = road_addCar(1);
   if (carId2 == -1) {
      fprintf(stderr, "Something went wrong when trying to add a car on the road. Exiting.\n");
      return EXIT_FAILURE;
   }

   pthread_t t;
   pthread_create(&t, NULL, avancer, &carId1);
   pthread_create(&t, NULL, avancer, &carId2);
   
   while (!road_isEscPressed())
   {
      //road_stepCar(carId1);
      //road_stepCar(carId2);
      road_refresh();
   }

   road_shutdown();

   return 0;
}