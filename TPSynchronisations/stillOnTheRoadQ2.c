#define _GNU_SOURCE
#include <unistd.h>             // For usleep
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>             // For rand
#include <semaphore.h>
#include <signal.h>
#include <limits.h>
#include <road.h>

pthread_mutex_t verrou = PTHREAD_MUTEX_INITIALIZER;

typedef struct {
    int carId;
    int roadId;                 // 0: left -> right; 1: right -> left
} CarInfo;

void *avancer(void *ci)
{
<<<<<<< HEAD
    CarInfo *carInfo = (CarInfo *) ci;
    const int carId = carInfo->carId;
    const int roadId = carInfo->roadId;

    bool outOfRoad = false;
    bool wait = false;

    while (!outOfRoad) {
        if (road_distToCross(carId) <= road_minDist) {
            if (!wait) {
                pthread_mutex_lock(&verrou);
                wait = true;
            }
            outOfRoad = !road_stepCar(carId);
            if (road_distToCross(carId) > road_minDist) {
                pthread_mutex_unlock(&verrou);
                wait = false;
            }
        } else 
            outOfRoad = !road_stepCar(carId);
            usleep(3000);
        

=======
   CarInfo* carInfo = (CarInfo*) ci;
   const int carId = carInfo->carId;
   const int roadId = carInfo->roadId;

   bool outOfRoad = false;

   while (!outOfRoad)
   {
       while (road_distToCross(carId)>0)
       {
            outOfRoad = !road_stepCar(carId);
            usleep(3000);
       }
       
       pthread_mutex_lock(&mut);
       
        while (road_distToCross(carId) != INT_MAX)
        {
           outOfRoad = !road_stepCar(carId);
           usleep(3000);
        }
        
        pthread_mutex_unlock(&mut);
        
        while (!outOfRoad)
        {
            outOfRoad = !road_stepCar(carId);
            usleep(3000);
        }
<<<<<<< HEAD
>>>>>>> 230eed3fc5eaa96362fa70a8408f2687e3647537
    }
    else
        outOfRoad = !road_stepCar(carId);
        usleep(3000);
    
        
    }
=======

      
   }

>>>>>>> 4f4d32664789508f07accbf37d1ba6d7b472df30

    road_removeCar(carId);

    return NULL;
}

void *createCars(void *unused)
{
    pthread_t carThread;
    const int nbCars = 300;
    CarInfo carsInfo[nbCars];
    const int minDelay = 300000;
    int delay;

    for (int i = 0; i < nbCars; i++) {
        delay = minDelay + rand() % (minDelay);
        usleep(delay);
        const int roadId = rand() > RAND_MAX / 3 ? 0 : 1;   // A bit more cars from left than from
                                                            // right
        carsInfo[i].carId = road_addCar(roadId);
        carsInfo[i].roadId = roadId;
        pthread_create(&carThread, NULL, avancer, &carsInfo[i]);
    }

    // Vu le délai entre le démarrage des threads et le fait qu'ils durent
    // a priori le même temps, je n'ai besoin de faire un "join" que sur le 
    // dernier thread
    pthread_join(carThread, NULL);

    return NULL;
}

int main(int argc, const char *argv[])
{
    road_init(1);

    pthread_t createCarsThread;

    pthread_create(&createCarsThread, NULL, createCars, NULL);

    while (!road_isEscPressed() && pthread_tryjoin_np(createCarsThread, NULL) == EBUSY) {
        usleep(1000);
        road_refresh();
    }

    road_shutdown();

    return 0;
}
